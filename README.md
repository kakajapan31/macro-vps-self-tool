# macro-vps-self-tool

###ytb
- run: python main_ytb.py

####how it work
- view random 1 video in list video in dontpad in random seconds
- next video after finish to view last video
- finish and go to other video


###ytb playlist
- run: python main_ytb_playlist.py

#### how it work
- view random playlist in list playlist of dontpad
- view playlist with n videos (n will be config in dontpad)
- each video, we will view in random seconds which we config
- after finish to view video, click next video in playlist
- finish and go to other playlist

###ytb music
- run: python main_ytb_music.py

#### how it work
- view random playlist in list playlist of dontpad in random seconds
- next playlist after finish to view playlist
- finish and go to other playlist


###update git
- run: python update_git.py

#### how it work
- pull last update code in gitlab repository


###update_config
- run: python update_config.py

#### how it work
- update config.txt in each command like config_info in dontpad
- commands/ytb/config.txt: url /kakajapan31_update-config-ytb
- commands/ytb_music/config.txt: url /kakajapan31_update-config-ytb-music


