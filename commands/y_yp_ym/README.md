#### ytb-dontpad:
- Dòng 1: thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo theo format: <link_youtube> <min_time_view> <max_time_view>

#### ytb-music
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo theo format: <link_ytb_music> <min_music> <max_music> <min_time_view> <max_time_view>

#### ytb-playlist
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo theo format: <link_ytb_playlist> <min_video> <max_video> <min_time_view> <max_time_view>
