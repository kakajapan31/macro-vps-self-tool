#### ytb-dontpad:
- Dòng 1: thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo config thời gian run trong ngày
- Các dòng tiếp theo theo format: <link_youtube>|<channel_name>|<min_next_video>|<max_next_video>|<min_time_view>|<max_time_view>

#### ytb-music
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo config thời gian run trong ngày
- Các dòng tiếp theo theo format: <link_ytb_music>|<min_music>|<max_music>|<min_time_view>|<max_time_view>

#### ytb-playlist
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo config thời gian run trong ngày
- Các dòng tiếp theo theo format: <link_ytb_playlist>|<min_video>|<max_video>|<min_time_view>|<max_time_view>

#### ytb-search
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo config thời gian run trong ngày
- Các dòng tiếp theo theo format: <link_youtube>|<search_info>|<channel_name>|<min_next_video>|<max_next_video>|<min_time_view>|<max_time_view>

#### ytb-end-screen
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo config thời gian run trong ngày
- Các dòng tiếp theo theo format: <link_youtube>|<min_next_video>|<max_next_video>|<min_time_view>|<max_time_view>|<min_time_click_end>|<max_time_click_end>

#### apl-music-search
- Dòng 1: Thời gian theo format: 2021-05-19T00:00:00
- Các dòng tiếp theo config thời gian run trong ngày
- Các dòng tiếp theo theo format: <search_info>|<min_time_view>|<max_time_view>

